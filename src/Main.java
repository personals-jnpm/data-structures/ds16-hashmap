import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;

public class Main {

    public static void main(String[] args) {
        Main main = new Main();

        // main.tabla();
        main.mapa();
    }

    public void tabla() {
        Hashtable<Integer, String> hm = new Hashtable<>();

        // Input the values
        hm.put(1, "Geeks");
        hm.put(12, "forGeeks");
        hm.put(15, "A computer");
        hm.put(3, "Portal");

        // Printing the Hashtable
        System.out.println(hm);
        System.out.println(hm.get(15));

        System.out.println(hm.getOrDefault(5, "La tabla no contiene la llave"));

        if (hm.containsValue("Portal")) {
            System.out.println("La tabla si lo contiene");
        } else {
            System.out.println("No lo contiene");
        }

        System.out.println(hm.keySet());
        for (String value : hm.values()) {
            System.out.println(value);
        }
        Enumeration<Integer> keys = hm.keys();
        for (int i = 0; i < hm.size(); i++) {
            System.out.println(hm.get(keys.nextElement()));
        }
    }

    public void mapa() {
        // Creates an empty HashMap
        HashMap<Integer, Integer> hashMap = new HashMap<>();

        int[] arr = {10, 34, 5, 10, 3, 5, 10};

        // Traverse through the given array
        for (int j : arr) {

            // Get if the element is present
            Integer c = hashMap.get(j);

            // If this is first occurrence of element
            // Insert the element
            if (hashMap.get(j) == null) {
                hashMap.put(j, 1);
            }

            // If elements already exists in hash map
            // Increment the count of element by 1
            else {
                hashMap.put(j, ++c);
            }
        }

        // Print HashMap
        System.out.println(hashMap);

        System.out.println(hashMap.size());
        hashMap.replace(5, 300);
        System.out.println(hashMap);

        hashMap.remove(3);
        System.out.println(hashMap);

        System.out.println(hashMap.values());
        hashMap.clear();
        System.out.println(hashMap.size());
        System.out.println(hashMap);
    }
}
